# Starter-ci3-boilerplate

## ¿Qué es CodeIgniter?

Este repositorio 'starter-ci3-boilerplate' es un template para
nuestros proyectos con Codeigniter nuestro potente framework de PHP 
construido para desarrolladores que necesitan un kit de herramientas
simple y elegante para crear aplicaciones web completas.

- Sitio oficial                    : [Sitio oficial](http://codeigniter.com).
- Repositorio oficial  Verisón 3   : [Repositorio](https://github.com/bcit-ci/CodeIgniter).
- Guía de usuario oficial Versión 3: [Aquí](https://codeigniter.com/userguide3/index.html). 

## Instalación

Para instalar el repositorio escoja o SSH o HTTPS

```bash
[SSH]$ git clone git@gitlab.com:cihispano.org/starter-ci3-boilerplate.git
```
```bash
[HTTPS]$ git clone https://gitlab.com/cihispano.org/starter-ci3-boilerplate.git
```

Una vez clonado el repositorio ejecute
```bash
$ composer install
```

Seguido instale las dependecias de Node
```bash
$ npm install
$ npm run dev
```

## Requerimientos del Servidor

Se recomienda PHP versión 5.6 o posterior.

Debería funcionar en 5.3.7 también, pero le recomendamos que NO ejecute
versiones antiguas de PHP, debido a la seguridad y el rendimiento potenciales
problemas, así como las características que faltan.

### Reconocimiento y Créditos

El equipo de CodeIgniter agradece a EllisLab, todos los colaboradores 
del proyecto CodeIgniter y a usted, el usuario de CodeIgniter. Así
como también a:

* Luis Mata [GitHub](https://gist.github.com/lalan21j) - Sistema para crear y generar las etiquetas 'Meta'.
* Cesar Cansino [Pórtal Oficial](http://www.cesarcancino.com/) - Sistema de Layout(modificado para este repo).