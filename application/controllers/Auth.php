<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	/**
	 * [__construct description]
	 */
	public function __construct() {
		parent::__construct();
		/**
		 * Instaciar siempre la librería para que el sistema de layout fucione correctamente.
		 */
		$this->layout->setLayout('auth');
	}

	/**
	 * Undocumented function
	 *
	 * @return void
	 */
	public function index() {
		/**
		 * Estructura para instanciar la vista requerida y que sea renderizada.
		 */
		$data['content'] = 'auth/login';
		$this->parser->parse('layouts/auth', $data);
	}

	/**
	 * Undocumented function
	 *
	 * @return void
	 */
	public function register() {
		$data['content'] = 'auth/register';
		$this->parser->parse('layouts/auth', $data);
	}

	/**
	 * Undocumented function
	 *
	 * @return void
	 */
	public function forget() {
		$data['content'] = 'auth/forget';
		$this->parser->parse('layouts/auth', $data);
	}
}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */