<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/home
	 *	- or -
	 * 		http://example.com/index.php/home/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/home/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
		parent::__construct();
		/**
         * Instaciar siempre la librería para que el sistema de layout fucione correctamente.
         */
		$this->layout->setLayout('app');
	}

	/**
	 * Undocumented function
	 *
	 * @return void
	 */
	public function index()
	{
		/**
		 * Estructura para instanciar la vista requerida y que sea renderizada.
		 */
		$data['content'] = 'home';
        $this->parser->parse('layouts/app', $data);
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */