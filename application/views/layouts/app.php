<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!doctype html>
<html lang="es" class="h-100">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $this->layout->getTitle(); ?></title>
        <?php
$meta = array(
	array('name' => 'author', 'content' => $this->layout->getAuthor()),
	array('name' => 'description', 'content' => $this->layout->getDescripcion()),
	array('name' => 'application-name', 'content' => $this->layout->getApplicationName()),
	array('name' => 'generator', 'content' => $this->layout->getGenerator()),
	array('name' => 'keywords', 'content' => $this->layout->getKeywords()),
	array('name' => 'robots', 'content' => $this->layout->getRobots()),
	array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0'),
	array('name' => 'apple-mobile-web-app-capable', 'content' => 'yes'),
	array('name' => 'google', 'content' => 'notranslate'),
	array('name' => 'google-site-verification', 'content' => $this->layout->getGoogle_Site_Verification()),
	array('name' => 'msvalidate.01', 'content' => $this->layout->getMsValidate()),
	array('name' => 'copyright', 'content' => 'Copyrigth'),
	array('name' => 'distribution', 'content' => 'global'),
	array('name' => 'x-ua-compatible', 'content' => 'ie=edge, chrome=1', 'type' => 'equiv'),
);
echo meta($meta);
?>
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('/dist/img/apple-icon-57x57.png'); ?>">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('/dist/img/apple-icon-60x60.png'); ?>">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('/dist/img/apple-icon-72x72.png'); ?>">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('/dist/img/apple-icon-76x76.png'); ?>">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('/dist/img/apple-icon-114x114.png'); ?>">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('/dist/img/apple-icon-120x120.png'); ?>">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('/dist/img/apple-icon-144x144.png'); ?>">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('/dist/img/apple-icon-152x152.png'); ?>">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('/dist/img/apple-icon-180x180.png'); ?>">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url('/dist/img/android-icon-192x192.png'); ?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('/dist/img/favicon-32x32.png'); ?>">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('/dist/img/favicon-96x96.png'); ?>">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('/dist/img/favicon-16x16.png'); ?>">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php echo base_url('/dist/img/favicon/ms-icon-144x144.png'); ?>">
        <meta name="theme-color" content="#ffffff">
        <!--  App CSS (Do not remove) -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('/dist/css/app.css'); ?>">
        <!-- CSS auxiliares -->
            <?php echo $this->layout->css; ?>
            <?php echo $this->layout->js; ?>
        <!-- fin auxiliares -->
    </head>
    <body class="d-flex justify-content-center align-items-center vh-100 bg-light">
        <header>
            <!-- Content of Header -->
        </header>
        <main role="main" class="flex-shrink-0" id="app">
            <!-- Contenido y carga dinámica de las vistas -->
            <?php $this->load->view($content);?>
        </main>
        <footer id="footer">
            <!-- Content of Footer -->
        </footer>
        <!-- App JS (Do not remove) -->
        <script src="<?php echo base_url('/dist/js/commons.js'); ?>"></script>
        <script src="<?php echo base_url('/dist/js/app.js'); ?>"></script>
    </body>
</html>