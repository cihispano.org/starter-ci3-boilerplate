import 'core-js/stable'
import Vue from 'vue'
import '@coreui/coreui'

// Installing whole package
import CoreuiVue from '@coreui/vue';
import '@coreui/icons'
Vue.use(CoreuiVue);

// Vue Toast Notification
import VueToast from 'vue-toast-notification';
Vue.use(VueToast, {
  	position: 'top-right',
  	duration: 3500,
    dismissible: true
});

// VeeValidate
import { ValidationObserver, ValidationProvider, extend, localize, configure } from 'vee-validate';
import es from 'vee-validate/dist/locale/es'
import * as rules from 'vee-validate/dist/rules';
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
localize('es', es);

// Importamos todas las reglas de validacion
Object.keys(rules).forEach(rule => {
  	extend(rule, rules[rule]);
});

const config = {
  	classes: {
    	valid: 'is-valid',
    	invalid: 'is-invalid'
  	}
};

// Sets the options.
configure(config);

// Favicon
import '../img/favicon/apple-icon-precomposed.png'
import '../img/favicon/apple-icon.png'
import '../img/favicon/apple-icon-57x57.png'
import '../img/favicon/apple-icon-60x60.png'
import '../img/favicon/apple-icon-72x72.png'
import '../img/favicon/apple-icon-76x76.png'
import '../img/favicon/apple-icon-114x114.png'
import '../img/favicon/apple-icon-120x120.png'
import '../img/favicon/apple-icon-144x144.png'
import '../img/favicon/apple-icon-152x152.png'
import '../img/favicon/apple-icon-180x180.png'
import '../img/favicon/android-icon-36x36.png'
import '../img/favicon/android-icon-48x48.png'
import '../img/favicon/android-icon-72x72.png'
import '../img/favicon/android-icon-96x96.png'
import '../img/favicon/android-icon-144x144.png'
import '../img/favicon/android-icon-192x192.png'
import '../img/favicon/favicon-16x16.png'
import '../img/favicon/favicon-32x32.png'
import '../img/favicon/favicon-96x96.png'
import '../img/favicon/ms-icon-70x70.png'
import '../img/favicon/ms-icon-144x144.png'
import '../img/favicon/favicon.ico'
// App
import '../scss/app'

/**
 * [app description]: Instanciamos Vue en 'app'
 * @type {Vue}
 */
new Vue({
    el: '#app',
    data: {
    	msg: 'Starter-ci3-boilerplate 1.0'
	}
})